
import java.util.Scanner;

class MinN  // название класса соответствует смыслу программы
{ 
	public static void main(String[] args)   // точка вхождения в программу, целочисленные переменные
	{	
		Scanner input = new Scanner (System.in);  // создание объекта, который будет считывать вводимое с клавиатуры
		
		int minimal_number = -1; //создаем переменную для сохранения мин. числа и, с учетом "флага" присваиваем ей (-1)
		int number = input.nextInt();  // в переменную number кладем вводимое с клавиатуры значение
		 
		while(number != -1)   // алгоритм продолжает работу до нахождения "флага" числа "-1"
		{	
			if  (minimal_number > number) { // найдено новое меньшее число 
			minimal_number = number; // новому меньшему числу последовательности присваиваем мин. значение
			number = input.nextInt();
			}
			else if (minimal_number <= number) { // новое  число не меньше нашего минимального числа
			minimal_number = minimal_number; // сохраняем наше минимальное число
			number = input.nextInt();
			}					
    	}	
    	System.out.println ("minimal number in sequence:" + minimal_number); //вывести минимальное число среди всей последовательности
    		
    }
}

