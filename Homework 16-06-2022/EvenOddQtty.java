
// Посчитать количество всех четных и нечетных чисел в последовательности, где последнее число в последовательности всегда (-1)

import java.util.Scanner;

class EvenOddQtty // название класса соответствует смыслу программы
{ 
	public static void main(String[] args)   // точка входа в программу, целочисленные переменные
	{				
		int count_even = 0;    // счетчик четных чисел с начальным значением '0'
		int count_odds = 0;    // счетчик нечетных чисел с начальным значением '0'

		Scanner input = new Scanner (System.in);  // создание объекта, который будет считывать вводимое с клавиатуры
		int number = input.nextInt();  // в переменную number кладем вводимое с клавиатуры значение
		 
		while(number != -1)   // алгоритм продолжает работу до нахождения "флага" числа "-1"
		{	
			int oneorzero = number%2; 
			if  (oneorzero == 0) { // число делится на "2" без остатка
			count_even = count_even + 1; // счетчик чётных чисел увеличивается на единицу
			number = input.nextInt();
			}
			else if (oneorzero == 1) { // число делится на "2" с остатком
			count_odds = count_odds + 1; // счетчик нечётных чисел увеличивается на единицу
			number = input.nextInt();
			}					
    	}	
    	System.out.println ("EVEN numbers total quantity in sequence is: " + count_even); // всего четных чисел во всей последовательности
    	System.out.println ("ODD numbers total quantity in sequence is: " + count_odds); // всего нечетных чисел во всей последовательности
    		
    }
}

